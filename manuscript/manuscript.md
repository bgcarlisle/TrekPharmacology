---
title: "Cordrazine or inaprovaline for emergency resuscitation in humanoids: a systematic review and meta-analysis"
author:
 - "Benjamin Gregory Carlisle PhD"
 - "Delwen Franzen PhD"
 - "Alan Blayney MSc"
csl: nature.csl
bibliography: references.bib
abstract: "Good emergency care depends on a robust evidence base on which to base medical practice. The evidence for cordrazine vs inaprovaline in emergency resuscitation is inconclusive. In the following, we report a systematic review and meta-analysis of observational reports of drug administration and outcomes in a therapeutic setting between the years 2151 and 2399. The primary outcome was OS-E (overall survival by the end of the episode), pooled as a fixed effects meta-analysis of proportions among patients receiving cordrazine vs inaprovaline for emergency resuscitation. As secondary outcomes, adverse events (AEs) were pooled as a fixed effects meta-analysis of proportions among patients receiving cordrazine vs inaprovaline for any indication. "
---

# Introduction

Good emergency care depends on a robust evidence base on which to base medical practice.
The evidence for cordrazine vs inaprovaline in emergency resuscitation is inconclusive.
A literature review found limited observational evidence[@HorburgerWherenoguideline2014] for cordrazine therapy and no evidence of any kind regarding inaprovaline in emergency resuscitation.
There were no published, ongoing or registered randomized controlled trials (RCTs) for either cordrazine or inaprovaline.

# Methods

A systematic review and meta-analysis of observational reports of drug administration and outcomes in a therapeutic setting between the years 2151 and 2399 was conducted.

## Data sources

All named drugs mentioned or used in a live-action *Star Trek* feature[@StarTrekTV;@StarTrekNext;@StarTrekDeep;@StarTrekVoyager;@StarTrekEnterprise] were screened and recorded.

## Inclusion and exclusion criteria

Drugs that have already been approved by the FDA prior to 2020 were excluded.
Drugs with medical indications for treatment of individuals were included.
Cosmetics (e.g. hypicate) and population-level interventions (e.g. hytritium) were excluded.
Instances of the use of cordrazine or inaprovaline therapy for emergency resuscitation were included in the meta-analysis.

## Data extraction

Drug-level data were single-extracted and cross-referenced with Memory Alpha to ensure accuracy.[@MemoryAlpha]
Drug names, doses, routes of administration, prescribing physician, nearest stardate, year and location of administration, recipient's species, indication, known adverse events, and approval status at time of administration were screened for inclusion and recorded.[@carlisle_cps_2020]

Indication of prescription and patient-level outcomes (survival and adverse events) for each instance of cordrazine or inaprovaline therapy were double-coded by two independent coders using Numbat Systematic Review Manager.[@numbat-carlisle]
Differences were resolved by single combat with a bat'leth.

Cases of drug administration that occurred within an alternate timeline, alternate reality, or a telepathic projection were included, and outcomes were recorded from the context of the reality as presented. (E.g. drug exposures that occurred within an alternate reality were counted, and deaths counted against overall survival, even if that character lived in the prime reality.)

## Primary outcome

The primary outcome was OS-E (overall survival by the end of the episode), pooled as a fixed effects meta-analysis of proportions among patients receiving cordrazine vs inaprovaline for emergency resuscitation.

## Secondary outcomes

Adverse events (AEs) were pooled as a fixed effects meta-analysis of proportions among patients receiving cordrazine vs inaprovaline for any indication.
As a sensitivity analysis, we also compared the pooled OS-E and AE proportions for cordrazine or tricordrazine vs inaprovaline.

## Statistics

The protocol,[@cordrazine_inaprovaline_osf_2020] complete data set[@carlisle_cps_2020] and code for this study[@CarlisleTrekPharmacology] are available online. 
All statistical analyses were carried out using R v. 3.6.3.[@R]
Meta-analysis of proportions was implemented by the *meta* package.[@r_meta]
We defined $p<0.05$ to be statistically significant.

# Results

A total of 155 instances of drug administration in a therapeutic context were screened, including 102 unique drugs administered to 68 individuals from 38 distinct humanoid species between the years 2151 and 2399.

Cordrazine and inaprovaline therapy were the most commonly administered therapies, with 15 recorded uses of each.
There were an additional 3 recorded uses of tricordrazine.
Thirteen cases of cordrazine use (87%), 10 cases of inaprovaline use (67%) and 2 cases of tricordrazine use (67%) were indicated for emergency resuscitation.

The proportions for OS-E among humanoids receiving cordrazine vs inaprovaline were 69% (95% CI 41-88%) vs 60% (95% CI 30-84%). 
The differences between these proportions was not statistically significant ($p=0.6457$, see Figure 1).
The pooled AE rate for cordrazine was 7% (95% CI 1-35%, see Figure 2).
There were no adverse events recorded for inaprovaline therapy, and so this endpoint could not be meta-analyzed or formally compared with the adverse event rate for cordrazine.

If tricordrazine is included in the meta-analysis with cordrazine, OS-E decreases to 67% (95% CI 41-85%) and the adverse event rate decreases to 6% (95% CI 1-32%).

# Discussion

Inaprovaline has a much safer adverse event profile than cordrazine.

Both administered by hypospray.

This observational study was not sufficiently powered to detect a difference in effect sizes, so more research is needed.

\newpage

# Tables

|                             |           Cordrazine |        Inaprovaline |
|-----------------------------|---------------------:|--------------------:|
| Instances of administration |                   13 |                  10 |
| Dates of administration     |            2267-2376 |           2366-2375 |
| Distinct patients           |                   12 |                  10 |
| Distinct species            |                    6 |                   8 |
| Doses administered (range)  | 0.1-25 cc / 10-50 mg | 20-75 cc / 20-75 mg |


Table 1. Sample properties for cordrazine vs inaprovaline therapy for emergency resuscitation in humanoids

\newpage

# Figures

![OS-E (overall survival by the end of the episode) for humanoids receiving cordrazine vs inaprovaline therapy](combined-os_e.pdf "Figure 1")

![Adverse events for cordrazine therapy in humanoids](cordrazine-ae.pdf "Figure 2")

\newpage

# References
