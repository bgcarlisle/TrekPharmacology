---
title: "Cordrazine or inaprovaline for emergency resuscitation in humanoids: a systematic review and meta-analysis"
author:
 - "Benjamin Gregory Carlisle PhD"
csl: nature.csl
bibliography: references.bib
abstract: "Good emergency care depends on a robust evidence base on which to base medical practice. The evidence for cordrazine vs inaprovaline in emergency resuscitation is inconclusive. In the following, we will conduct a systematic review and meta-analysis of observational reports of drug administration and outcomes in a therapeutic setting between the years 2151 and 2399. The primary outcome is OS-E (overall survival by the end of the episode), pooled as a fixed effects meta-analysis of proportions among patients receiving cordrazine vs inaprovaline for emergency resuscitation. As secondary outcomes, adverse events (AEs) will be pooled as a fixed effects meta-analysis of proportions among patients receiving cordrazine vs inaprovaline for any indication. As a sensitivity analysis, we will also compare the pooled OS-E and AE proportions for cordrazine or tricordrazine vs inaprovaline."
---

# Background

Good emergency care depends on a robust evidence base on which to base medical practice.
The evidence for cordrazine vs inaprovaline in emergency resuscitation is inconclusive.
A literature review found limited observational evidence[@HorburgerWherenoguideline2014] for cordrazine therapy and no evidence of any kind regarding inaprovaline in emergency resuscitation.
There were no published, ongoing or registered randomized controlled trials (RCTs) for either cordrazine or inaprovaline.

# Methods overview

A systematic review and meta-analysis of observational reports of drug administration and outcomes in a therapeutic setting between the years 2151 and 2399.

## Data sources

All named drugs mentioned or used in a live-action *Star Trek* feature[@StarTrekTV;@StarTrekNext;@StarTrekDeep;@StarTrekVoyager;@StarTrekEnterprise] were previously screened.

## Inclusion and exclusion criteria

Drugs that have already been approved by the FDA prior to 2020 are excluded.
Drugs with medical indications for treatment of individuals are included.
Cosmetics (e.g. hypicate) and population-level interventions (e.g. are excluded.
Instances of the use of cordrazine or inaprovaline therapy for emergency resuscitation were included in the meta-analysis.

## Data extraction

Drug-level data were single-extracted and cross-referenced with Memory Alpha to ensure accuracy.[@MemoryAlpha]
Drug names, doses, routes of administration, prescribing physician, nearest stardate, year of administration, recipient's species, indication, known adverse events, and approval status at time of administration were extracted.

Indication of prescription and patient-level outcomes (survival and adverse events) for each instance of cordrazine or inaprovaline therapy will be double-coded by two independent coders using Numbat Systematic Review Manager.[@numbat-carlisle]
Differences will be resolved by single combat with a bat'leth.

## Primary outcome

The primary outcome is OS-E (overall survival by the end of the episode), pooled as a fixed effects meta-analysis of proportions among patients receiving cordrazine vs inaprovaline for emergency resuscitation.

## Secondary outcomes

Adverse events (AEs) will be pooled as a fixed effects meta-analysis of proportions among patients receiving cordrazine vs inaprovaline for any indication.
As a sensitivity analysis, we will also compare the pooled OS-E and AE proportions for cordrazine or tricordrazine vs inaprovaline.

## Statistics

The complete data set[@carlisle_cps_2020] and code for this study[@CarlisleTrekPharmacology] are available online. 
All statistical analyses will be carried out using R v. 3.6.3.[@R]
Meta-analysis of proportions will be implemented by the *meta* package.[@r_meta]
We define $p<0.05$ to be statistically significant.

# Notes

This protocol was registered after screening but before completion of patient outcome extraction.

# References
