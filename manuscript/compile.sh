#!/bin/bash
FILE1=$1
cd "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )";
pandoc "$FILE1" --filter pandoc-citeproc --reference-doc=style.docx -o "output/$(date +"%Y-%m-%d %R") Manuscript.docx";
pandoc "$FILE1" --filter pandoc-citeproc -o "output/$(date +"%Y-%m-%d %R") Manuscript.pdf";

