library(tidyverse)
library(meta)

cases <- read_tsv("2020-11-13-numbat-export.tsv")

cases$id <- NULL
cases$id_1 <- NULL
cases$manual <- NULL
cases$timestamp_started <- NULL
cases$refsetid <- NULL
cases$referenceid <- NULL
cases$userid <- NULL # Won't need this for the final version
cases$status <- NULL
cases$notes <- NULL

series <- tribble (
    ~series, ~order,
    "TOS", 1,
    "TNG", 2,
    "DS9", 3,
    "VOY", 4,
    "ENT", 5,
    "DSC", 6,
    "PIC", 7
)

get_ae_numerator_for_series_and_drug <- function (ser, dru) {

    cases %>%
        filter (series == ser) %>%
        filter (drug_name == dru) %>%
        filter (drug_administered == 1) %>%
        filter (ae == "yes") %>%
        nrow () %>%
        return ()
    
}

get_ae_denominator_for_series_and_drug <- function (ser, dru) {

    cases %>%
        filter (series == ser) %>%
        filter (drug_name == dru) %>%
        filter (drug_administered == 1) %>%
        nrow () %>%
        return ()
    
}

series$ae_numerator_cordrazine <- mapply(
    get_ae_numerator_for_series_and_drug,
    series$series,
    "cordrazine"
)

series$ae_denominator_cordrazine <- mapply(
    get_ae_denominator_for_series_and_drug,
    series$series,
    "cordrazine"
)

series$ae_numerator_inaprovaline <- mapply(
    get_ae_numerator_for_series_and_drug,
    series$series,
    "inaprovaline"
)

series$ae_denominator_inaprovaline <- mapply(
    get_ae_denominator_for_series_and_drug,
    series$series,
    "inaprovaline"
)

cordrazine_include <- series %>%
    filter (ae_denominator_cordrazine > 0)

cordrazine <- metaprop (
    ae_numerator_cordrazine,
    ae_denominator_cordrazine,
    series,
    data = cordrazine_include
)

pdf (
    "cordrazine-ae.pdf",
    8, 3
)

forest (
    cordrazine,
    sortorder = order,
    comb.random = FALSE,
    overall.hetstat = FALSE
)

dev.off()

png (
    "cordrazine-ae.png",
    8, 3,
    units = "in",
    res = 500
)

forest (
    cordrazine,
    sortorder = order,
    comb.random = FALSE,
    overall.hetstat = FALSE
)

dev.off()

inaprovaline_include <- series %>%
    filter (ae_denominator_inaprovaline > 0)

inaprovaline <- metaprop (
    ae_numerator_inaprovaline,
    ae_denominator_inaprovaline,
    series,
    data = inaprovaline_include
)

forest (
    inaprovaline,
    sortorder = order,
    comb.random = FALSE
    
)

