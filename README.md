# CPS: Compendium of Pharmaceuticals, Starfleet

This is the code for the web app that manages my listing of all the drugs from live-action *Star Trek* features (available at [cps.bgcarlisle.com](https://cps.bgcarlisle.com).
This is also version control for the manuscript and statistical analysis for *Cordrazine or inaprovaline for emergency resuscitation in humanoids: a systematic review and meta-analysis*.

## What this isn't

This is not where you can contribute to the data set on which [cps.bgcarlisle.com](https://cps.bgcarlisle.com) is based.
Please click the button marked "Suggest a correction (error or omission)" on the site directly.
You may also download the full data set from the site directly.
(Note that it is provided on a non-commercial, by-attribution Creative Commons license.)

## How to set an instance of this site up

This web app was designed to work on a LAMP stack.

To make this thing work, set up a MySQL database with the database schema described in `db-schema.sql`.

Then, copy the `web/` directory into a folder on your web server.

Edit `config.php` such that it has your database credentials, the absolute path to the root of the folder you just copied everything from `web/` into, the URL of the site that corresponds to that folder, and an email that your server can send from, so that you'll be notified if someone clicks the "Send a correction" button.

That should do it!

## Citing The Compendium of Pharmaceuticals, Starfleet

Here is a BibTeX entry for *The Compendium of Pharmaceuticals, Starfleet*:

```
@website{carlisle_cps_2020,
	location = {{Retrieved from https://cps.bgcarlisle.com/}},
	title = {Compendium of Pharmaceuticals, Starfleet},
	url = {https://cps.bgcarlisle.com/},
	organization = {{The Grey Literature}},
	date = {2020-10},
	author = {Carlisle, Benjamin Gregory}
}
```

If you used this data or code and you found it useful, I would take it as a kindness if you cited it.

Best,

Benjamin Gregory Carlisle PhD
