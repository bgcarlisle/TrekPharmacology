<?php

if (admin_save_new_entry ($_POST['drug_name'], $_POST['dose'], $_POST['route'], $_POST['prescribed_by'], $_POST['stardate'], $_POST['year'], $_POST['location_of_care'], $_POST['recipient_name'], $_POST['recipient_gender'], $_POST['recipient_species'], $_POST['indication'], $_POST['known_adverse_events'], $_POST['approval_status'], $_POST['series'], $_POST['season'], $_POST['episode'], $_POST['time_index'])) {

    make_backup ();

    export_tsv ();
    
    $success_notice = '<div class="alert alert-success" role="alert">Successfully saved new entry</div>';
} else {
    $success_notice = '<div class="alert alert-danger" role="alert">Error in saving new entry</div>';
}

?>
