<?php

include_once ("../config.php");

if ( isset ($_POST['action']) ) {

    switch ($_POST['action']) {

	case "add-new":
	    include (ABS_PATH . "admin/add-new.php");
	    break;

	case "edit":
	    include (ABS_PATH . "admin/edit.php");
	    break;
    }
}

$rows = get_admin_page_table ();
$corrections = get_corrections ();

?><!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>CPS: Compendium of Pharmaceuticals, Starfleet</title>

	<!-- Bootstrap -->
	<link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet">

	<!-- CPS css -->
	<link href="<?php echo SITE_URL; ?>cps.css" rel="stylesheet">

	<meta name="author" content="Benjamin Gregory Carlisle PhD Email: murph@bgcarlisle.com Website: https://www.bgcarlisle.com/ Social media: https://scholar.social/@bgcarlisle">

    </head>
    <body>
	<nav class="nav">
	    <a class="nav-link" href="<?php echo SITE_URL; ?>">Home</a>
	    <a class="nav-link" href="https://codeberg.org/bgcarlisle/TrekPharmacology" target="_blank">Source</a>
	    <a class="nav-link" href="https://scholar.social/@bgcarlisle" target="_blank">Social media</a>
	    <a class="nav-link" href="<?php echo SITE_URL; ?>admin/">Admin</a>
	</nav>
	<div id="citeMask" class="hidden" onclick="hide_dialog (event);">&nbsp;</div>
	<div id="addnew" class="notes">
	    <button class="btn btn-secondary btn-sm" style="float: right;" onclick="hide_dialog(event);">Close</button>
	    <h2>Add new entry</h2>
	    <form action="<?php echo SITE_URL; ?>admin/" method="post">
		<input type="hidden" name="action" value="add-new">
		<div class="form-group">
		    <label for="type">Drug name</label>
		    <input type="text" class="form-control" id="drug_name" name="drug_name" aria-describedby="drugnameHelp">
		    <small id="drugnameHelp" class="form-text text-muted">E.g. cordrazine</small>
		</div>
		<div class="form-group">
		    <label for="type">Dose</label>
		    <input type="text" class="form-control" id="dose" name="dose" aria-describedby="doseHelp">
		    <small id="doseHelp" class="form-text text-muted">E.g. 5 cc</small>
		</div>
		<div class="form-group">
		    <label for="type">Route</label>
		    <input type="text" class="form-control" id="route" name="route" aria-describedby="routeHelp">
		    <small id="routeHelp" class="form-text text-muted">E.g. hypospray</small>
		</div>
		<div class="form-group">
		    <label for="type">Prescribed by</label>
		    <input type="text" class="form-control" id="prescribed_by" name="prescribed_by" aria-describedby="prescribedbyHelp">
		    <small id="prescribedbyHelp" class="form-text text-muted">E.g. McCoy</small>
		</div>
		<div class="form-group">
		    <label for="type">Stardate</label>
		    <input type="text" class="form-control" id="stardate" name="stardate" aria-describedby="stardateHelp">
		    <small id="stardateHelp" class="form-text text-muted">E.g. 3134.0</small>
		</div>
		<div class="form-group">
		    <label for="type">Year</label>
		    <input type="text" class="form-control" id="year" name="year" aria-describedby="yearHelp">
		    <small id="yearHelp" class="form-text text-muted">E.g. 2267</small>
		</div>
		<div class="form-group">
		    <label for="type">Location of care</label>
		    <input type="text" class="form-control" id="location_of_care" name="location_of_care" aria-describedby="locationHelp">
		    <small id="locationHelp" class="form-text text-muted">E.g. sick bay, infirmary, hospital, starship, away mission</small>
		</div>
		<div class="form-group">
		    <label for="type">Recipient</label>
		    <input type="text" class="form-control" id="recipient_name" name="recipient_name" aria-describedby="recipientHelp">
		    <small id="recipientHelp" class="form-text text-muted">E.g. Sulu</small>
		</div>
		<div class="form-group">
		    <label for="type">Gender</label>
		    <input type="text" class="form-control" id="recipient_gender" name="recipient_gender" aria-describedby="genderHelp">
		    <small id="genderHelp" class="form-text text-muted">E.g. woman, man, etc.</small>
		</div>
		<div class="form-group">
		    <label for="type">Species</label>
		    <input type="text" class="form-control" id="recipient_species" name="recipient_species" aria-describedby="speciesHelp">
		    <small id="speciesHelp" class="form-text text-muted">E.g. Human</small>
		</div>
		<div class="form-group">
		    <label for="type">Indication</label>
		    <input type="text" class="form-control" id="indication" name="indication" aria-describedby="indicationHelp">
		    <small id="indicationHelp" class="form-text text-muted">E.g. Emergency resuscitation</small>
		</div>
		<div class="form-group">
		    <label for="type">Known adverse events</label>
		    <input type="text" class="form-control" id="known_adverse_events" name="known_adverse_events" aria-describedby="aeHelp">
		    <small id="aeHelp" class="form-text text-muted">E.g. Fatal to humans</small>
		</div>
		<div class="form-group">
		    <label for="type">Approval status</label>
		    <input type="text" class="form-control" id="approval_status" name="approval_status" aria-describedby="approvalHelp">
		    <small id="approvalHelp" class="form-text text-muted">E.g. Experimental</small>
		</div>
		<div class="form-group">
		    <label for="type">Series</label>
		    <input type="text" class="form-control" id="series" name="series" aria-describedby="seriesHelp">
		    <small id="seriesHelp" class="form-text text-muted">E.g. TOS</small>
		</div>
		<div class="form-group">
		    <label for="type">Season</label>
		    <input type="text" class="form-control" id="season" name="season" aria-describedby="seasonHelp">
		    <small id="seasonHelp" class="form-text text-muted">E.g. 3</small>
		</div>
		<div class="form-group">
		    <label for="type">Episode</label>
		    <input type="text" class="form-control" id="episode" name="episode" aria-describedby="episodeHelp">
		    <small id="episodeHelp" class="form-text text-muted">E.g. 14</small>
		</div>
		<div class="form-group">
		    <label for="type">Approximate time (h:mm:ss)</label>
		    <input type="text" class="form-control" id="time_index" name="time_index" aria-describedby="time_indexHelp">
		    <small id="time_indexHelp" class="form-text text-muted">E.g. 0:25:35</small>
		</div>

		<button class="btn btn-block btn-primary" style="margin-bottom: 40px;">Save</button>
		
	    </form>
	</div>
	<?php

	// edit dialog boxes

	foreach ($rows as $edit_dialog) {
	?><div class="notes" id="edit-<?php echo $edit_dialog['id']; ?>">
	    <button class="btn btn-secondary btn-sm" style="float: right;" onclick="hide_dialog(event);">Close without saving</button>
	    <h2>Edit entry</h2>
	    <form action="<?php echo SITE_URL; ?>admin/" method="post">
		<input type="hidden" name="action" value="edit">
		<input type="hidden" name="did" value="<?php echo $edit_dialog['id']; ?>">
		<div class="form-group">
		    <label for="type">Drug name</label>
		    <input type="text" class="form-control" id="drug_name" name="drug_name" aria-describedby="drugnameHelp" value="<?php echo $edit_dialog['drug_name']; ?>">
		    <small id="drugnameHelp" class="form-text text-muted">E.g. cordrazine</small>
		</div>
		<div class="form-group">
		    <label for="type">Dose</label>
		    <input type="text" class="form-control" id="dose" name="dose" aria-describedby="doseHelp" value="<?php echo $edit_dialog['dose']; ?>">
		    <small id="doseHelp" class="form-text text-muted">E.g. 5 cc</small>
		</div>
		<div class="form-group">
		    <label for="type">Route</label>
		    <input type="text" class="form-control" id="route" name="route" aria-describedby="routeHelp" value="<?php echo $edit_dialog['route']; ?>">
		    <small id="routeHelp" class="form-text text-muted">E.g. hypospray</small>
		</div>
		<div class="form-group">
		    <label for="type">Prescribed by</label>
		    <input type="text" class="form-control" id="prescribed_by" name="prescribed_by" aria-describedby="prescribedbyHelp" value="<?php echo $edit_dialog['prescribed_by']; ?>">
		    <small id="prescribedbyHelp" class="form-text text-muted">E.g. McCoy</small>
		</div>
		<div class="form-group">
		    <label for="type">Stardate</label>
		    <input type="text" class="form-control" id="stardate" name="stardate" aria-describedby="stardateHelp" value="<?php echo $edit_dialog['stardate']; ?>">
		    <small id="stardateHelp" class="form-text text-muted">E.g. 3134.0</small>
		</div>
		<div class="form-group">
		    <label for="type">Year</label>
		    <input type="text" class="form-control" id="year" name="year" aria-describedby="yearHelp" value="<?php echo $edit_dialog['year']; ?>">
		    <small id="yearHelp" class="form-text text-muted">E.g. 2267</small>
		</div>
		<div class="form-group">
		    <label for="type">Location of care</label>
		    <input type="text" class="form-control" id="location_of_care" name="location_of_care" aria-describedby="locationHelp" value="<?php echo $edit_dialog['location_of_care']; ?>">
		    <small id="locationHelp" class="form-text text-muted">E.g. sick bay, infirmary, hospital, starship, away mission</small>
		</div>
		<div class="form-group">
		    <label for="type">Recipient</label>
		    <input type="text" class="form-control" id="recipient_name" name="recipient_name" aria-describedby="recipientHelp" value="<?php echo $edit_dialog['recipient_name']; ?>">
		    <small id="recipientHelp" class="form-text text-muted">E.g. Sulu</small>
		</div>
		<div class="form-group">
		    <label for="type">Gender</label>
		    <input type="text" class="form-control" id="recipient_gender" name="recipient_gender" aria-describedby="genderHelp" value="<?php echo $edit_dialog['recipient_gender']; ?>">
		    <small id="genderHelp" class="form-text text-muted">E.g. woman, man, etc.</small>
		</div>
		<div class="form-group">
		    <label for="type">Species</label>
		    <input type="text" class="form-control" id="recipient_species" name="recipient_species" aria-describedby="speciesHelp" value="<?php echo $edit_dialog['recipient_species']; ?>">
		    <small id="speciesHelp" class="form-text text-muted">E.g. Human</small>
		</div>
		<div class="form-group">
		    <label for="type">Indication</label>
		    <input type="text" class="form-control" id="indication" name="indication" aria-describedby="indicationHelp" value="<?php echo str_replace ("\"", "&quot;", $edit_dialog['indication']); ?>">
		    <small id="indicationHelp" class="form-text text-muted">E.g. Emergency resuscitation</small>
		</div>
		<div class="form-group">
		    <label for="type">Known adverse events</label>
		    <input type="text" class="form-control" id="known_adverse_events" name="known_adverse_events" aria-describedby="aeHelp" value="<?php echo str_replace("\"", "&quot;", $edit_dialog['known_adverse_events']); ?>">
		    <small id="aeHelp" class="form-text text-muted">E.g. Fatal to humans</small>
		</div>
		<div class="form-group">
		    <label for="type">Approval status</label>
		    <input type="text" class="form-control" id="approval_status" name="approval_status" aria-describedby="approvalHelp" value="<?php echo $edit_dialog['approval_status']; ?>">
		    <small id="approvalHelp" class="form-text text-muted">E.g. Experimental</small>
		</div>
		<div class="form-group">
		    <label for="type">Series</label>
		    <input type="text" class="form-control" id="series" name="series" aria-describedby="seriesHelp" value="<?php echo $edit_dialog['series']; ?>">
		    <small id="seriesHelp" class="form-text text-muted">E.g. TOS</small>
		</div>
		<div class="form-group">
		    <label for="type">Season</label>
		    <input type="text" class="form-control" id="season" name="season" aria-describedby="seasonHelp" value="<?php echo $edit_dialog['season']; ?>">
		    <small id="seasonHelp" class="form-text text-muted">E.g. 3</small>
		</div>
		<div class="form-group">
		    <label for="type">Episode</label>
		    <input type="text" class="form-control" id="episode" name="episode" aria-describedby="episodeHelp" value="<?php echo $edit_dialog['episode']; ?>">
		    <small id="episodeHelp" class="form-text text-muted">E.g. 14</small>
		</div>
		<div class="form-group">
		    <label for="type">Approximate time (h:mm:ss)</label>
		    <input type="text" class="form-control" id="time_index" name="time_index" aria-describedby="time_indexHelp" value="<?php echo $edit_dialog['time_index']; ?>">
		    <small id="time_indexHelp" class="form-text text-muted">E.g. 0:25:44</small>
		</div>
		
		<button class="btn btn-block btn-primary" style="margin-bottom: 40px;">Save</button>
		
	    </form>
	</div>
	<?php
	}
	
	// delete dialog boxes
	
	foreach ($rows as $delete_dialog) {
	?><div class="notes" id="delete-<?php echo $delete_dialog['id']; ?>">
	    <h2>Delete <?php echo $delete_dialog['drug_name']; ?> (<?php echo $delete_dialog['series']; ?> <?php echo $delete_dialog['season']; ?>x<?php echo $delete_dialog['episode']; ?>)?</h2>
	    <p>This cannot be undone.</p>
	    <button class="btn btn-danger" onclick="delete_drug(event, <?php echo $delete_dialog['id']; ?>);">Delete</button>
	    <button class="btn btn-secondary" onclick="hide_dialog(event);">Do not delete</button>
	</div>
	<?php
	}

	// delete corrections

	foreach ($corrections as $delcorr) {
	?><div class="notes" id="delcorr-<?php echo $delcorr['id']; ?>">
	    <h2>Delete suggestion correction</h2>
	    <p>Submitter: <?php echo $delcorr['submitter']; ?></p>
	    <p>Suggested correction: <?php echo str_replace("\n", "<br>", $delcorr['suggestion']); ?></p>
	    <p>When submitted: <?php echo $delcorr['when_submitted']; ?></p>
	    <button class="btn btn-danger" onclick="delete_correction(event, <?php echo $delcorr['id']; ?>);">Delete</button>
	    <button class="btn btn-secondary" onclick="hide_dialog(event);">Do not delete</button>
	</div>
	<?php } ?>
	<div class="container-fluid">
	    <div class="row">
		<div class="col">
		    <h1>CPS admin</h1>
		    <?php
		    echo $success_notice;
		    ?>
		    <button class="btn btn-primary" onclick="show_addnew(event);" style="margin-bottom: 40px;">Add new</button>
		</div>
	    </div>
	    <div class="row">
		<div class="col-md-6">
		    <div class="input-group" style="margin-bottom: 40px;">
			<input type="text" id="filter-table-input" class="form-control" placeholder="Filter table" aria-label="Filter table" aria-describedby="filter-table-button" value="<?php echo $_GET['s']; ?>">
			<div class="input-group-append">
			    <button class="btn btn-primary" type="button" id="filter-table-button" onclick="event.preventDefault();filter_table();">Filter table</button>
			    
			</div>
		    </div>
		</div>
		<div class="col-md-6">
		    <button class="btn btn-secondary btn-block" type="button" onclick="$('#filter-table-input').val('');event.preventDefault();filter_table();" style="margin-bottom: 40px;">Show all</button>
		</div>
	    </div>
	    <?php

	    if ( count ($corrections) > 0 ) {
	    ?><div class="row">
		<div class="col">
		    <h2>Suggested corrections</h2>
		</div>
	    </div>
	    <div class="row">
		<div class="col">
		    <div class="table-responsive">
			<table class="table table-striped table-hover table-sm">
			    <thead class="thead-light">
				<tr>
				    <th scope="col">Submitter</th>
				    <th scope="col">Correction</th>
				    <th scope="col" class="righttable">When submitted</th>
				    <th scope="col" class="righttable">Delete</th>
				</tr>
			    </thead>
			    <tbody>
				<?php

				foreach ( $corrections as $corr ) {
				?><tr id="corr-<?php echo $corr['id']; ?>">
				    <td><?php echo $corr['submitter']; ?></td>
				    <td><?php echo str_replace ("\n", "<br>", $corr['suggestion']);  ?></td>
				    <td class="righttable"><?php echo $corr['when_submitted']; ?></td>
				    <td class="righttable"><button class="btn btn-sm btn-danger" onclick="delete_correction_prompt(event, <?php echo $corr['id']; ?>);">Delete</button></td>
				</tr><?php
				     }
				     
				     ?>
			    </tbody>
			</table>
		    </div>
		</div>
	    </div><?php
		  }
		  
		  ?>
	    <div class="row">
		<div class="col">
		    <h2>Drugs</h2>
		</div>
	    </div>
	    <div class="row">
		<div class="col">
		    <div class="table-responsive">
			<table class="table table-striped table-hover table-sm">
			    <thead class="thead-light">
				<tr>
				    <th scope="col">Drug</th>
				    <th scope="col" class="righttable">Published</th>
				    <th scope="col" class="righttable">Edit</th>
				    <th scope="col" class="righttable">Delete</th>
				</tr>
			    </thead>
			    <tbody>
				<?php

				foreach ($rows as $row) {

				    if ($row['published'] == 1) {
					$published = '<td class="righttable table-success" id="published-' . $row['id'] .'" onclick="update_published(event, ' .$row['id'] . ');">Yes</td>';
				    } else {
					$published = '<td class="righttable table-danger" id="published-' . $row['id'] .'" onclick="update_published(event, ' . $row['id'] . ');">No</td>';
				    }

				?><tr id="row-<?php echo $row['id']; ?>">
				<th scope="row"><?php echo $row['recipient_name'];  ?> <?php echo $row['drug_name']; ?> <?php echo $row['series']; ?> <?php echo $row['season'] ?>x<?php echo $row['episode']; ?> <?php echo $row['time_index']; ?></th>
				<?php echo $published; ?>
				<td class="righttable"><button class="btn btn-sm btn-secondary" onclick="edit_drug(event, <?php echo $row['id']; ?>);">Edit</button></td>
				<td class="righttable"><button class="btn btn-sm btn-danger" onclick="delete_prompt(event, <?php echo $row['id']; ?>);">Delete</button></td>
				</tr><?php
				     
				     }
				     
				     ?>
			    </tbody>
			</table>
			
		    </div>
		</div>
	    </div>
	</div>
	
	<!-- jQuery -->
	<script src="<?php echo SITE_URL; ?>jquery-3.4.1.min.js"></script>

	<!-- Popper.js -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.bundle.min.js"></script>

	<!-- Bootstrap JS -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.min.js"></script>

	<!-- CPS JS -->
	<script>
	 function show_addnew (event) {
	     event.preventDefault();
	     $('#citeMask').fadeIn(400, function () {
		 $('#addnew').slideDown();
	     });
	 }
	 
	 function hide_dialog (event) {
	     event.preventDefault();
	     $('.notes').slideUp(0, function () {
		 $('#citeMask').fadeOut();
	     });
	 }

	 function delete_prompt (event, drug_id) {
	     event.preventDefault();

	     $('#citeMask').fadeIn(400, function () {
		 $('#delete-' + drug_id).slideDown();
	     });
	 }

	 function delete_correction_prompt (event, drug_id) {
	     event.preventDefault();

	     $('#citeMask').fadeIn(400, function () {
		 $('#delcorr-' + drug_id).slideDown();
	     });
	 }

	 function delete_drug (event, drug_id) {
	     event.preventDefault();

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>admin/delete.php',
		 type: 'post',
		 data: {
		     did: drug_id
		 },
		 dataType: 'html'
	     }).done ( function (response) {
		 if ( response != 'Error' ) {
		     $('#row-' + drug_id).addClass('table-danger');
		     hide_dialog(event);
		     setTimeout (function () {
			 $('#row-' + drug_id).remove();
		     }, 3000);
		 }
	     });
	 }

	 function delete_correction (event, correction_id) {
	     event.preventDefault();

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>admin/delete-correction.php',
		 type: 'post',
		 data: {
		     cid: correction_id
		 },
		 dataType: 'html'
	     }).done ( function (response) {
		 if ( response != 'Error' ) {
		     $('#corr-' + correction_id).addClass('table-danger');
		     hide_dialog(event);
		     setTimeout (function () {
			 $('#corr-' + correction_id).remove();
		     }, 3000);
		 }
	     });
	 }

	 function edit_drug (event, drug_id) {
	     event.preventDefault();
	     
	     hide_dialog(event);

	     $('#citeMask').fadeIn(400, function () {
		 $('#edit-' + drug_id).slideDown();
	     });
	     
	 }

	 function update_published (event, drug_id) {

	     $.ajax ({
		 url: '<?php echo SITE_URL; ?>admin/published.php',
		 type: 'post',
		 data: {
		     did: drug_id
		 },
		 dataType: 'html'
	     }).done ( function (response) {
		 if ( response != 'MySQL Error' ) {

		     $('#published-' + drug_id).html(response);

		     switch (response) {
			 case 'No':
			     $('#published-' + drug_id).removeClass('table-success');
			     $('#published-' + drug_id).addClass('table-danger');
			     break;
			 case 'Yes':
			     $('#published-' + drug_id).removeClass('table-danger');
			     $('#published-' + drug_id).addClass('table-success');
			     break;
		     }
		     
		 } else {
		     alert (response);
		 }
	     });
	     
	 }

	 function filter_table () {

	     query = $('#filter-table-input').val();

	     if ( query == '' ) {
		 $('tbody th').parent().fadeIn(0);
	     } else {
		 $('tbody th').parent().fadeOut(0);

		 $('tbody th').each( function (index) {

		     cell_value = $(this).html();

		     if (cell_value.toLowerCase().search(query.toLowerCase()) != -1) {
			 $(this).parent().fadeIn(0);
		     }
		     
		 });
		 
	     }
	     

	 }

	 $('#filter-table-input').on('input', function (e) {
	     filter_table ();
	 });

	 $('div.notes input:text').on('change', function (e) {

	     value = $(this).val();

	     if ( value != '' ) {
		 $(this).removeClass('alert-danger');
	     } else {
		 $(this).addClass('alert-danger');
	     }
	     
	 });

	 $(document).ready(function () {
	     if ( $('#filter-table-input').val() != '' ) {
		 filter_table();
	     }

	     $('div.notes input:text').filter(function() { return $(this).val() == ""; }).addClass('alert-danger');
	 });

	</script>
    </body>
</html>
