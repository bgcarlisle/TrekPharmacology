<?php

include_once ("config.php");

if ( isset ( $_POST['drug'] ) ) {
    include ("correction.php");
}

$rows = get_front_page_table ();

?><!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>CPS: Compendium of Pharmaceuticals, Starfleet</title>

	<!-- Bootstrap -->
	<link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet">

	<!-- CPS css -->
	<link href="<?php echo SITE_URL; ?>cps.css" rel="stylesheet">

	<meta name="author" content="Benjamin Gregory Carlisle PhD Email: murph@bgcarlisle.com Website: https://www.bgcarlisle.com/ Social media: https://scholar.social/@researchfairy">

    </head>
    <body>
	<nav class="nav">
	    <a class="nav-link" href="#" onclick="show_citation(event);">Cite</a>
	    <a class="nav-link" href="https://codeberg.org/bgcarlisle/TrekPharmacology" target="_blank">Source</a>
	    <a class="nav-link" href="https://scholar.social/@researchfairy" target="_blank">Social media</a>
	    <a class="nav-link" href="<?php echo SITE_URL; ?>admin/">Admin</a>
	</nav>
	<div id="citeMask" class="hidden" onclick="hide_dialog (event);">&nbsp;</div>
	<div id="cite" class="notes">
	    <button class="btn btn-secondary btn-sm" style="float: right;" onclick="hide_dialog(event);">Close</button>
	    <h2>How to cite The Compendium of Pharmaceuticals, Starfleet</h2>
	    <h3>BibTeX</h3>
	    <pre>
@website{carlisle_cps_2020,
location = {{Retrieved from https://cps.bgcarlisle.com/}},
title = {Compendium of Pharmaceuticals, Starfleet},
url = {https://cps.bgcarlisle.com/},
organization = {{The Grey Literature}},
date = {2020-10},
author = {Carlisle, Benjamin Gregory}
}
	    </pre>
	    <h3>Vancouver</h3>
	    <p>Carlisle BG. Compendium of Pharmaceuticals, Starfleet [Internet]. Retrieved from https://cps.bgcarlisle.com/: The Grey Literature; 2020. Available from: https://cps.bgcarlisle.com/</p>
	    <h3>AMA</h3>
	    <p>Carlisle BG. <i>Compendium of Pharmaceuticals, Starfleet</i>. Retrieved from https://cps.bgcarlisle.com/: The Grey Literature; 2020. https://cps.bgcarlisle.com/.</p>
            <h3>MLA</h3>
            <p>Carlisle, Benjamin Gregory. <i>Compendium of Pharmaceuticals, Starfleet.</i> The Grey Literature, 2020, https://cps.bgcarlisle.com/.</p>
        </div>
	<div class="notes" id="correction">
	    <button class="btn btn-secondary btn-sm" style="float: right;" onclick="hide_dialog(event);">Close</button>
	    <h2>Contact me</h2>
	    <p>Please indicate the entry to be corrected (if any), what needs to be fixed, and provide a short justification for why.</p>
	    <form action="<?php echo SITE_URL; ?>" method="post">
		<div class="form-group">
		    <label for="drug">What entry needs to be corrected? (Leave blank in the case of an omission)</label>
		    <select class="form-control" id="drug" name="drug">
			<option value="na"></option>
			<?php

			foreach ($rows as $drug) {

			?><option value="<?php echo $drug['id']; ?>"><?php echo ucfirst ( $drug['drug_name'] ); ?> (<?php echo $drug['series']; ?> <?php echo $drug['season']; ?>x<?php echo $drug['episode']; ?>)</option><?php } ?>
		    </select>
		    <small id="drugHelp" class="form-text text-muted"></small>
		</div>


		<div class="form-group">
		    <label for="suggestion">Suggested correction</label>
		    <textarea class="form-control" id="suggestion" name="suggestion" rows="5"></textarea>
		    <small id="suggestionHelp" class="form-text text-muted">Please indicate the error or omission and provide a brief explanation, if necessary.</small>
		</div>

		<div class="form-group">
		    <label for="submitter">Your email</label>
		    <input type="text" class="form-control" id="submitter" name="submitter" aria-describedby="submitterHelp">
		    <small id="submitterHelp" class="form-text text-muted">Optional; but if I have questions and I can't contact you, your suggested correction might just get deleted. For privacy reasons, all suggested corrections (including the submitter's email address) are deleted from the database automatically after 72 hours.</small>
		</div>

		<button class="btn btn-block btn-primary" style="margin-bottom: 40px;">Suggest correction</button>
		
	    </form>
	</div>
	<div class="container-fluid">
	    <div class="row">
		<div class="col">
		    <?php echo $success_notice; ?>
		    <h1>CPS: Compendium of Pharmaceuticals, Starfleet</h1>
		    <p style="font-style: italic;">Benjamin Gregory Carlisle PhD</p>
		    <img src="cc-by-nc.png" style="margin-bottom: 20px;" title="This license lets others remix, adapt, and build upon this work non-commercially, and although their new works must also acknowledge Dr Carlisle and be non-commercial, they don’t have to license their derivative works on the same terms.">
		    <a class="btn btn-primary btn-sm" href="<?php echo SITE_URL; ?>cps.tsv" style="margin-bottom: 20px;">Download this data set</a>
		    <p>Inclusion and exclusion criteria</p>
		    <ul>
			<li>All named drugs (e.g. "cordrazine" from TOS S01E28 would be included, but "neural paralyzer" from TOS S02E05 would not be included)</li>
			<li>Mentioned or used in a live-action Star Trek feature</li>
			<li>Not a real-life drug (e.g. digoxin; norep), and not a real-life drug with one letter replaced for legal reasons (e.g. mexhohexital)</li>
			<li>For a treatment of a medical condition (no cosmetics, e.g. hypicate)</li>
			<li>For use on individuals (e.g. hytritium was used to purify a colony's water table)</li>
		    </ul>

		    <button class="btn btn-secondary" onclick="show_correct(event);" style="margin-bottom: 40px;">Suggest a correction (error or omission)</button>
		</div>
	    </div>
	    <div class="row">
		<div class="col-md-6">
		    <div class="form-group">
			<label for="drug">Filter by species</label>
			<select class="form-control" id="species_filter" name="species_filter" onchange="filter_by_species();">
			    <option value="na"></option>
			    <?php

			    $species = [];

			    foreach ($rows as $drug) {
				array_push($species, $drug['recipient_species']);
			    }

			    $species = array_unique($species);

			    asort($species);

			    foreach ($species as $sp) { ?>
				<option value="<?php echo str_replace( " ", "", preg_replace("/[^A-Za-z0-9 ]/", '', $sp)); ?>"><?php echo $sp; ?></option>
			    <?php } ?>

			</select>
			<small id="drugHelp" class="form-text text-muted"></small>
		    </div>
		</div>
		<div class="col-md-6">
		    <div class="form-group">
			<label for="drug">Filter by series</label>
			<select class="form-control" id="series_filter" name="series_filter" onchange="filter_by_series();">
			    <option value="na"></option>
			    <?php

			    $series = [];

			    foreach ($rows as $drug) {
				array_push($series, $drug['series']);
			    }

			    $series = array_unique($series);

			    asort($series);

			    foreach ($series as $se) { ?>
				<option value="<?php echo preg_replace("/[^A-Za-z0-9 ]/", '', $se); ?>"><?php echo $se; ?></option>
			    <?php } ?>

			</select>
			<small id="drugHelp" class="form-text text-muted"></small>
		    </div>
		</div>
	    </div>
	    <div class="row">
		<div class="col-md-6">
		    <div class="input-group" style="margin-bottom: 40px;">
			<input type="text" id="filter-table-input" class="form-control" placeholder="Filter table by drug name" aria-label="Filter table" aria-describedby="filter-table-button" value="<?php echo $_GET['s']; ?>">
			<div class="input-group-append">
			    <button class="btn btn-primary" type="button" id="filter-table-button" onclick="event.preventDefault();filter_table();">Search</button>
			    
			</div>
		    </div>
		</div>
		<div class="col-md-6">
		    <button class="btn btn-secondary btn-block" type="button" onclick="$('#filter-table-input').val('');event.preventDefault();filter_table();" style="margin-bottom: 40px;">Show all</button>
		</div>
	    </div>
	    <div class="row">
		<div class="col">
		    <table class="table table-striped table-hover table-sm">
			<thead class="thead-light">
			    <tr>
				<th scope="col">Drug name</th>
				<th scope="col">Dose</th>
				<th scope="col">Route</th>
				<th scope="col">Prescribed by</th>
				<th scope="col">Stardate</th>
				<th scope="col">Year</th>
				<th scope="col">Location of care</th>
				<th scope="col">Recipient</th>
				<th scope="col">Gender</th>
				<th scope="col">Species</th>
				<th scope="col">Indication</th>
				<th scope="col">Known adverse events</th>
				<th scope="col">Approval status</th>
				<th scope="col">Episode</th>
				<th scope="col">Time</th>
			    </tr>
			</thead>
			<tbody>
			    <?php foreach ($rows as $row) {

				$class = str_replace (" ", "", preg_replace("/[^A-Za-z0-9 ]/", '', $row['recipient_species'])) . " " . $row['series'];

			    ?>
				<tr class="<?php echo $class; ?>">
				    <th scope="row"><?php echo $row['drug_name']; ?></th>
				    <td><?php echo $row['dose']; ?></td>
				    <td><?php echo $row['route']; ?></td>
				    <td><?php echo $row['prescribed_by']; ?></td>
				    <td><?php echo $row['stardate']; ?></td>
				    <td><?php echo $row['year']; ?></td>
				    <td><?php echo $row['location_of_care']; ?></td>
				    <td><?php echo $row['recipient_name']; ?></td>
				    <td><?php echo $row['recipient_gender']; ?></td>
				    <td><?php echo $row['recipient_species']; ?></td>
				    <td><?php echo $row['indication']; ?></td>
				    <td><?php echo $row['known_adverse_events']; ?></td>
				    <td><?php echo $row['approval_status']; ?></td>
				    <td><?php

					echo $row['series'];

					if ($row['season'] != 0) {
					    echo " S" . $row['season'] . "E" . $row['episode'];
					}
					
					?></td>
				    <td><?php echo $row['time_index']; ?></td>
				</tr>
			    <?php } ?>
			</tbody>
		    </table>
		</div>
	    </div>
	    <div class="row">
		<div class="col">
		    <h2>Abbreviations used</h2>
		    <p>TOS = The Original Series, TNG = The Next Generation, DS9 = Deep Space Nine, VOY = Voyager, ENT = Enterprise, DSC = Discovery, PIC = Picard, ST1 = Star Trek The Motion Picture, ST2 = Star Trek II, ST3 = Star Trek III, ns = Not stated.</p>
		</div>
	    </div>
	</div>
	
	<!-- jQuery -->
	<script src="<?php echo SITE_URL; ?>jquery-3.4.1.min.js"></script>

	<!-- Popper.js -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.bundle.min.js"></script>

	<!-- Bootstrap JS -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.min.js"></script>

	<!-- CPS JS -->
	<script>
	 function show_citation (event) {
	     event.preventDefault();
	     $('#citeMask').fadeIn(400, function () {
		 $('#cite').slideDown();
	     });
	 }

	 function hide_dialog (event) {
	     event.preventDefault();
	     $('.notes').slideUp(0, function () {
		 $('#citeMask').fadeOut();
	     });
	 }

	 function show_submit (event) {
	     event.preventDefault();
	     $('#citeMask').fadeIn(400, function () {
		 $('#submit').slideDown();
	     });
	 }

	 function show_correct (event) {
	     event.preventDefault();
	     $('#citeMask').fadeIn(400, function () {
		 $('#correction').slideDown();
	     });	     
	 }

	 function filter_table () {

	     $('#series_filter').val('na');
	     $('#species_filter').val('na');

	     query = $('#filter-table-input').val();

	     if ( query == '' ) {
		 $('tbody th').parent().fadeIn(0);
	     } else {
		 $('tbody th').parent().fadeOut(0);

		 $('tbody th').each( function (index) {

		     cell_value = $(this).html();

		     if (cell_value.toLowerCase().search(query.toLowerCase()) != -1) {
			 $(this).parent().fadeIn(0);
		     }
		     
		 });
		 
	     }
	     

	 }

	 function filter_by_species () {
	     species = $('#species_filter').val();

	     $('#series_filter').val('na');
	     $('#filter-table-input').val('');

	     if ( species == "na" ) {
		 filter_table();
	     } else {
		 $('tbody tr').fadeOut(0);
		 $('tbody tr.' + species).fadeIn(0);
	     }
	     
	 }

	 function filter_by_series () {
	     series = $('#series_filter').val();

	     $('#species_filter').val('na');
	     $('#filter-table-input').val('');

	     if ( series == "na" ) {
		 filter_table();
	     } else {
		 $('tbody tr').fadeOut(0);
		 $('tbody tr.' + series).fadeIn(0);
	     }
	     
	 }

	 $('#filter-table-input').on('input', function (e) {
	     filter_table ();
	 });

	 $(document).ready(function () {
	     if ( $('#filter-table-input').val() != '' ) {
		 filter_table();
	     }
	 });

	</script>
    </body>
</html>
