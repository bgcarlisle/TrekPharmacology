<?php

function get_front_page_table () {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("SELECT * FROM `drugs` WHERE `published` = 1 ORDER BY `drug_name` ASC, `year` ASC, `stardate` ASC;");

	$stmt->execute();

	$result = $stmt->fetchAll();

	return $result;
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function get_admin_page_table () {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("SELECT * FROM `drugs` ORDER BY `drug_name` ASC, `year` ASC, `stardate` ASC;");

	$stmt->execute();

	$result = $stmt->fetchAll();

	return $result;
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function get_single_entry ( $drug_id ) {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("SELECT * FROM `drugs` WHERE `id` = :did LIMIT 1;");

	$stmt->bindParam(':did', $did);

	$did = $drug_id;

	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result[0];
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function suggest_correction ( $drug_id, $suggestion, $submitter ) {
    
    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("INSERT INTO `corrections` (`drug_id`, `suggestion`, `submitter`) VALUES (:did, :sug, :sub)");

	$stmt->bindParam(':did', $did);
	$stmt->bindParam(':sug', $sug);
	$stmt->bindParam(':sub', $sub);

	if ( $drug_id != "na" ) {
	    $did = $drug_id;
	} else {
	    $did = NULL;
	}

	$sug = $suggestion;
	$sub = $submitter;

	if ($stmt->execute()) {

	    if ( $drug_id != "na" ) {
		
		$drug = get_single_entry ($drug_id);
		
		$subject = "CPS: Suggested correction to " . $drug['drug_name'] . " (" . $drug['series'] . " " . $drug['season'] . "x" . $drug['episode'] . ")";

		$body = "A suggested correction has been posted regarding " . $drug['drug_name'] . " (" . $drug['series'] . " " . $drug['season'] . "x" . $drug['episode'] . ")\n\n";

		$body = $body . "DETAILS FOR ORIGINAL SUBMISSION\n\n";
		$body = $body . "Drug name: " . $drug['drug_name'] . "\n";
		$body = $body . "Dose: " . $drug['dose'] . "\n";
		$body = $body . "Route: " . $drug['route'] . "\n";
		$body = $body . "Prescribed by: " . $drug['prescribed_by'] . "\n";
		$body = $body . "Stardate: " . $drug['stardate'] . "\n";
		$body = $body . "Year: " . $drug['year'] . "\n";
		$body = $body . "Recipient: " . $drug['recipient_name'] . "\n";
		$body = $body . "Species: " . $drug['recipient_species'] . "\n";
		$body = $body . "Indication: " . $drug['indication'] . "\n";
		$body = $body . "Known adverse events: " . $drug['known_adverse_events'] . "\n";
		$body = $body . "Approval status: " . $drug['approval_status'] . "\n";
		$body = $body . "Series: " . $drug['series'] . "\n";
		$body = $body . "Season: " . $drug['season'] . "\n";
		$body = $body . "Episode: " . $drug['episode'] . "\n";
		
	    } else {
		$subject = "CPS: Suggested correction";

		$body = "A suggestion correction has been posted with no entry selected.\n\n";
	    }

	    $body = $body . "DETAILS FOR SUGGESTED CORRECTION\n\n";

	    $body = $body . "Suggested correction submitter: " . $submitter . "\n\nSuggestion: " . $suggestion;

	    $headers = array (
		'From' => ADMIN_EMAIL
	    );
	    
	    mail(
		ADMIN_EMAIL,
		$subject,
		$body,
		$headers
	    );
	    
	    return TRUE;
	} else {
	    return FALSE;
	}
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function delete_old_corrections () {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("DELETE FROM `corrections` WHERE `when_submitted` < NOW() - INTERVAL 3 DAY;");

	$stmt->bindParam(':cid', $cid);

	$cid = $correction_id;

	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}



function get_corrections () {
    
    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("SELECT * FROM `corrections` ORDER BY `when_submitted` DESC;");

	$stmt->execute();

	$result = $stmt->fetchAll();

	return $result;
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function make_backup () {
    exec('mysqldump --user=' . DB_USER . ' --password=' . DB_PASS . ' --host=' . DB_HOST . ' ' . DB_NAME . ' > ' . '/home/bgc_cps/backups/' . date('Y-m-d-H-i') . '.sql' );
}

function export_tsv () { // ***
    exec ( "mysql -u " . DB_USER . " -p" . DB_PASS . " -h " . DB_HOST . " " . DB_NAME . " -B -e \"SELECT drug_name, dose, route, prescribed_by, stardate, year, location_of_care, recipient_name, recipient_gender, recipient_species, indication, known_adverse_events, approval_status, series, season, episode, time_index FROM drugs WHERE published = 1 ORDER BY drug_name ASC, year ASC, stardate ASC;\" > " . ABS_PATH . "cps.tsv" );
}

function admin_save_new_entry ($drug_name, $dose, $route, $prescribed_by, $stardate, $year, $location_of_care, $recipient_name, $recipient_gender, $recipient_species, $indication, $known_adverse_events, $approval_status, $series, $season, $episode, $time_index) {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("INSERT INTO `drugs` (`series`, `season`, `episode`, `time_index`, `stardate`, `year`, `location_of_care`, `prescribed_by`, `dose`, `route`, `drug_name`, `recipient_name`, `recipient_gender`, `recipient_species`, `indication`, `known_adverse_events`, `approval_status`) VALUES (:series, :season, :episode, :time_index, :stardate, :year, :location_of_care, :prescribed_by, :dose, :route, :drug_name, :recipient_name, :recipient_gender, :recipient_species, :indication, :known_adverse_events, :approval_status);");

	$stmt->bindParam(':series', $ser);
	$stmt->bindParam(':season', $sea);
	$stmt->bindParam(':episode', $epi);
	$stmt->bindParam(':time_index', $tim);
	$stmt->bindParam(':stardate', $sta);
	$stmt->bindParam(':location_of_care', $loc);
	$stmt->bindParam(':year', $yea);
	$stmt->bindParam(':prescribed_by', $pre);
	$stmt->bindParam(':dose', $dos);
	$stmt->bindParam(':route', $rou);
	$stmt->bindParam(':drug_name', $dru);
	$stmt->bindParam(':recipient_name', $ren);
	$stmt->bindParam(':recipient_gender', $reg);
	$stmt->bindParam(':recipient_species', $res);
	$stmt->bindParam(':indication', $ind);
	$stmt->bindParam(':known_adverse_events', $kae);
	$stmt->bindParam(':approval_status', $app);

	$ser = $series;
	$sea = $season;
	$epi = $episode;
	$tim = $time_index;
	$sta = $stardate;
	$yea = $year;
	$loc = $location_of_care;
	$pre = $prescribed_by;
	$dos = $dose;
	$rou = $route;
	$dru = $drug_name;
	$ren = $recipient_name;
	$reg = $recipient_gender;
	$res = $recipient_species;
	$ind = $indication;
	$kae = $known_adverse_events;
	$app = $approval_status;
	
	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function delete_drug ( $drug_id ) {

    
    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("DELETE FROM `drugs` WHERE `id` = :did LIMIT 1;");

	$stmt->bindParam(':did', $did);

	$did = $drug_id;

	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function delete_correction ($correction_id) {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("DELETE FROM `corrections` WHERE `id` = :cid LIMIT 1;");

	$stmt->bindParam(':cid', $cid);

	$cid = $correction_id;

	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function admin_save_edit ($drug_id, $drug_name, $dose, $route, $prescribed_by, $stardate, $year, $location_of_care, $recipient_name, $recipient_gender, $recipient_species, $indication, $known_adverse_events, $approval_status, $series, $season, $episode, $time_index) {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("UPDATE `drugs` SET `drug_name` = :drug_name, `dose` = :dose, `route` = :route, `prescribed_by` = :prescribed_by, `stardate` = :stardate, `year` = :year, `location_of_care` = :location_of_care, `recipient_name` = :recipient_name, `recipient_gender` = :recipient_gender, `recipient_species` = :recipient_species, `indication` = :indication, `known_adverse_events` = :known_adverse_events, `approval_status` = :approval_status, `series` = :series, `season` = :season, `episode` = :episode, `time_index` = :time_index WHERE `id` = :did");

	$stmt->bindParam(':series', $ser);
	$stmt->bindParam(':season', $sea);
	$stmt->bindParam(':episode', $epi);
	$stmt->bindParam(':time_index', $tim);
	$stmt->bindParam(':stardate', $sta);
	$stmt->bindParam(':year', $yea);
	$stmt->bindParam(':location_of_care', $loc);
	$stmt->bindParam(':prescribed_by', $pre);
	$stmt->bindParam(':dose', $dos);
	$stmt->bindParam(':route', $rou);
	$stmt->bindParam(':drug_name', $dru);
	$stmt->bindParam(':recipient_name', $ren);
	$stmt->bindParam(':recipient_gender', $reg);
	$stmt->bindParam(':recipient_species', $res);
	$stmt->bindParam(':indication', $ind);
	$stmt->bindParam(':known_adverse_events', $kae);
	$stmt->bindParam(':approval_status', $app);
	$stmt->bindParam(':did', $did);
	
	$ser = $series;
	$sea = $season;
	$epi = $episode;
	$tim = $time_index;
	$sta = $stardate;
	$yea = $year;
	$loc = $location_of_care;
	$pre = $prescribed_by;
	$dos = $dose;
	$rou = $route;
	$dru = $drug_name;
	$ren = $recipient_name;
	$reg = $recipient_gender;
	$res = $recipient_species;
	$ind = $indication;
	$kae = $known_adverse_events;
	$app = $approval_status;
	$did = $drug_id;
	
	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function update_published ( $drug_id ) {

    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("SELECT * FROM `drugs` WHERE `id` = :did;");

	$stmt->bindParam(':did', $did);

	$did = $drug_id;

	$stmt->execute();

	$result = $stmt->fetchAll();

	$drug = $result[0];
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    if ($drug['published'] == 0) {
	$newval = 1;
	$display = "Yes";
    } else {
	$newval = 0;
	$display = "No";
    }


    try {

	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$stmt = $dbh->prepare("UPDATE `drugs` SET `published` = :newval WHERE `id` = :did LIMIT 1;");

	$stmt->bindParam(':newval', $nv);
	$stmt->bindParam(':did', $did);

	$nv = $newval;
	$did = $drug_id;

	if ($stmt->execute()) {

	    return $display;
	    
	} else {

	    return "MySQL Error";
	    
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

?>
